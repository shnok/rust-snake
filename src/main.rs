extern crate sdl2_window;
extern crate window;
extern crate input;
extern crate graphics;
extern crate opengl_graphics;
extern crate rand;

use sdl2_window::Sdl2Window;
use window::{ WindowSettings };
use input::*;
use event_loop::*;
use graphics::*;
use opengl_graphics::*;
use rand::Rng;

#[derive(Clone, PartialEq)]
pub enum Direction {
    LEFT,
    RIGHT,
    UP,
    DOWN
}

pub const GRID_SIZE: f64 = 15.00;
pub const SQUARE_SIZE: f64 = 15.00;
pub const SPEED: f64 = 0.2;

pub struct Game {
    gl: GlGraphics,
    last_update: f64,
    direction: Direction,
    d_direction: Direction,
    story: Vec<(f64, f64)>,
    pixel_length: f64,
    p_coor: (usize, usize),
    f_coor: (usize, usize),
    p_pos: (f64, f64),
    speed: f64,
}

impl Game {
    fn render(&mut self, args: &RenderArgs) {
        let draw_state: DrawState = Default::default();
        let head = Rectangle::new(color::WHITE);
        let player = Rectangle::new(color::hex("FFFF00"));
        let food = Rectangle::new(color::hex("FF0000"));

        self.gl.draw(args.viewport(), |_c, g| {
            graphics::clear([0.0, 0.0, 0.0, 1.0], g);
        });

        let square = [self.get_position(self.f_coor.0) + 2.00, self.get_position(self.f_coor.1) + 2.00, SQUARE_SIZE - 4.00, SQUARE_SIZE - 4.00];
        self.gl.draw(args.viewport(), |c, g| {
            food.draw(square, &draw_state, c.transform, g);
        });

        for i in 0..self.story.len() {
            let square = [self.story[i].0 + 2.00, self.story[i].1 + 2.00, SQUARE_SIZE - 4.00, SQUARE_SIZE - 4.00];
            self.gl.draw(args.viewport(), |c, g| {
                player.draw(square, &draw_state, c.transform, g);
            });
        }

        let square = [self.p_pos.0 + 2.00, self.p_pos.1 + 2.00, SQUARE_SIZE - 4.00, SQUARE_SIZE - 4.00];
        self.gl.draw(args.viewport(), |c, g| {
            head.draw(square, &draw_state, c.transform, g);
        });
    }

    fn update(&mut self, args: &UpdateArgs) {
        self.last_update = &self.last_update + args.dt;
        if self.last_update >= self.speed {
            self.last_update = 0.0;

            self.update_pos();
            self.check_coordinates();
        }
    }

    fn new_game(&mut self) {
        self.p_coor = self.random_pos();
        self.p_pos = (self.get_position(self.p_coor.0), self.get_position(self.p_coor.1));
        self.story = vec![];
        self.new_food();
    }

    fn get_position(&self, coor:usize) -> f64 {
        coor as f64 * SQUARE_SIZE
    }

    fn check_coordinates(&mut self) {
        let mut x = 0.0;
        if self.p_pos.0 != 0.0 {
            x = self.p_pos.0 / SQUARE_SIZE;
        }
        self.p_coor.0 = x as usize;

        let mut y = 0.0;
        if self.p_pos.1 != 0.0 {
            y = self.p_pos.1 /  SQUARE_SIZE;
        }
        self.p_coor.1 = y as usize;

        if self.p_coor.0 == self.f_coor.0 && self.p_coor.1 == self.f_coor.1 {
            self.story.insert(0, (self.p_pos.0, self.p_pos.1));
            self.new_food();
            println!("Delicious!");
        } else if self.story.len() > 0 && self.is_player(self.p_pos, self.story.len()-1) {
            self.new_game();
            println!("Game over!");
        }
    }

    fn is_player(&mut self, pos:(f64, f64), range: usize) -> bool {
        if self.story.len() > 0 {
            if let Some(_index) = self.story[0..range].iter().position(|&i| i == (pos.0, pos.1)) {
                return true;
            }
            return false;
        }
        return false;
    }

    fn clamp(&self, mut val:f64) -> f64 {
        if val < 0.0 {
            val = self.pixel_length - SQUARE_SIZE;
        } else if val >= self.pixel_length {
            val = 0.0;
        }
        val
    }

    fn update_pos(&mut self) {
        if self.story.len() > 0 {
            self.story.drain(0..1);
            self.story.push((self.p_pos.0, self.p_pos.1));
        }

        self.direction = self.d_direction.clone();
        match self.direction {
            Direction::LEFT => self.p_pos.0 = &self.p_pos.0 - SQUARE_SIZE,
            Direction::RIGHT => self.p_pos.0 = &self.p_pos.0 + SQUARE_SIZE,
            Direction::DOWN => self.p_pos.1 = &self.p_pos.1 + SQUARE_SIZE,
            Direction::UP => self.p_pos.1 = &self.p_pos.1 - SQUARE_SIZE,
        }

        self.p_pos.0 = self.clamp(self.p_pos.0);
        self.p_pos.1 = self.clamp(self.p_pos.1);
    }

    fn random_pos(&mut self) -> (usize, usize){
        let mut rng = rand::thread_rng();
        let x = rng.gen_range(0, GRID_SIZE as usize);
        let y = rng.gen_range(0, GRID_SIZE as usize);
        return (x, y);
    }

    fn new_food(&mut self) {
        let mut new_pos = self.random_pos();
        while self.is_player((self.get_position(new_pos.0), self.get_position(new_pos.1)), self.story.len()) == true ||
            new_pos.0 == self.p_coor.0 && new_pos.1 == self.p_coor.1  {
            new_pos = self.random_pos();
        }
        self.f_coor = new_pos;
        println!("New food at {},{}", new_pos.0, new_pos.1);
    }

    fn change_direction(&mut self, button:Button) {
        if button == Button::Keyboard(Key::Left) {
            if self.story.len() == 0 || self.direction != Direction::RIGHT {
                self.d_direction = Direction::LEFT;
            }
        } else if button == Button::Keyboard(Key::Right) {
            if self.story.len() == 0 || self.direction != Direction::LEFT {
                self.d_direction = Direction::RIGHT;
            }
        } else if button == Button::Keyboard(Key::Up) {
            if self.story.len() == 0 || self.direction != Direction::DOWN {
                self.d_direction = Direction::UP;
            }
        } else if button == Button::Keyboard(Key::Down) {
            if self.story.len() == 0 || self.direction != Direction::UP {
                self.d_direction = Direction::DOWN;
            }
        }
    }
}

fn main() {
    let mut window: Sdl2Window = WindowSettings::new("Snake", (GRID_SIZE * SQUARE_SIZE, GRID_SIZE * SQUARE_SIZE))
        .fullscreen(false)
        .exit_on_esc(true)
        .build()
        .unwrap();

    let mut game = Game {
        gl: GlGraphics::new(OpenGL::V3_2),
        direction: Direction::RIGHT,
        d_direction: Direction::RIGHT,
        last_update: 0.0,
        p_coor: (0, 0),
        p_pos: (0.0, 0.0),
        f_coor: (0, 0),
        story: vec![],
        pixel_length: SQUARE_SIZE as f64 * GRID_SIZE as f64,
        speed: SPEED
    };

    game.new_game();

    let mut events = Events::new(EventSettings::new());
    while let Some(event) = events.next(&mut window) {
        if let Some(args) = event.render_args() {
            game.render(&args);
        }

        if let Some(u) = event.update_args() {
            game.update(&u);
        }

        if let Some(button) = event.press_args() {
            game.change_direction(button);
        }
    }
}
